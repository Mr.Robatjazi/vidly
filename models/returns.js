const Joi = require('joi');

function validateReturns(returns) {
    const schema = {
      movieId: Joi.objectId().required(),
      customerId: Joi.objectId().required(),
    };
  
    return Joi.validate(returns, schema);
}

module.exports.validate = validateReturns
  