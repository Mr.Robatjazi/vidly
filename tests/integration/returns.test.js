
const moment = require('moment');
const request =  require('supertest');
const {Rental} = require('../../models/rental');
const {Movie} = require('../../models/movie');
const {User} = require('../../models/user');

const mongoose = require('mongoose');

describe('/api/returns', ()=> {
    let server;
    let customerId;
    let movieId;
    let rental;
    let token;
    let movie;

    const exec = () => {
        return  request(server)
            .post('/api/returns')
            .set('x-auth-token', token)
            .send({  customerId, movieId });
    };
        
        
    beforeEach( async ()=> { 
        server=require('../../index');

        customerId = new mongoose.Types.ObjectId();
        movieId = new mongoose.Types.ObjectId();
        token = new User().generateAuthToken();

        movie  = new Movie( {
            _id: movieId,
            title:'123456',
            genre: {name: '123456'},
            numberInStock: 10,
            dailyRentalRate: 2,
        }) ;
        await movie.save();

        rental = new Rental({
            customer : {
                _id: customerId,
                name: '123456',
                phone: '123456',
            },
            movie: {
                _id: movieId,
                title: '1234567586',
                dailyRentalRate: 2
            } 
        })
        await rental.save();
        
    });

    afterEach(async ()=> { 
        await server.close();
        await Rental.remove({});
        await Movie.remove({});
    });

  
    // it ('should work! ' , async () => {
    //     const result = await Rental.findById(rental._id);
    //     expect(result).not.toBeNull();
    // })

    it ('should return 401 if customet not logged in' , async () => {
        token='';
        const res = await exec(); 
        expect(res.status).toBe(401);
    })

    it ('should return 400 if custometId not provided' , async () => {
        customerId='';
        const res = await exec(); 
        expect(res.status).toBe(400);
    })

    it ('should return 400 if movieId not provided' , async () => {
        movieId='';
        const res = await exec(); 
        expect(res.status).toBe(400);
    })
    
    it ('should return 404 if there is no rental for this customer/movie ' , async () => {
        await Rental.remove({});
        const res = await exec(); 
        expect(res.status).toBe(404);
    })

    it ('should return 400 if this rental already processed ' , async () => {
        rental.dateReturned= Date.now();
        await rental.save();
        const res = await exec(); 
        expect(res.status).toBe(400);
    })

    it ('should return 200 if this is a valid reauest ' , async () => {
        const res = await exec(); 
        expect(res.status).toBe(200);
    })

    it ('should set Return Date  ' , async () => {
        const res = await exec(); 
        const rentalInDb = await Rental.findById(rental._id);
        const diff = new Date() -  rentalInDb.dateReturned;
        //expect(res.body.dateReturned).not.toBeNull();
        expect(diff).toBeLessThan(10 *1000);
    })

    it ('should calculate the Rental Fee' , async () => {

        rental.dateOut = moment().add(-7,'days').toDate();
        await rental.save();
        const res = await exec(); 
        const rentalInDb = await Rental.findById(rental._id);

        expect(rentalInDb.rentalFee).toBe(14);
    })
    
    it ('should update the stock' , async () => {

        const res = await exec(); 
        const movieInDb = await Movie.findById(movieId);
        expect(movieInDb.numberInStock).toBe(movie.numberInStock + 1 );
    })

    it ('should return the Rental if request is valid' , async () => {

        const res = await exec(); 
        const rentalInDb = await Rental.findById(rental._id);
        expect(Object.keys(res.body)).toEqual(expect.arrayContaining(['movie', 'customer', 'dateOut', 'dateReturned']))
    })
    
});

