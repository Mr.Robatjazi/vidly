const  request = require('supertest');
const {Genre} = require('../../models/genre');

let server;

describe('api/genres' , () => {
    
    describe('GET /', ()=> {
        
        beforeEach(()=> { 
            server=require('../../index')
        });
        afterEach(async ()=> { 
            await server.close();
            await Genre.remove({});
         });
    
         it('should return no genres ', async () => {
      
            const res = await request(server).get('/api/genres/');
            expect(res.status).toBe(200);
            expect(res.text).toBe('No generes defined yet!');
            
        });          
            
        it('should return all genres', async () => {
            await Genre.collection.insertMany([
                {name: 'genre1'}  ,
                {name: 'genre2'}  ,
                {name: 'genre3'}  ,
              ]);          
      
            const res = await request(server).get('/api/genres/');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(3);
            expect(res.body.some(g => g.name==='genre1')).toBeTruthy();
        });          
    });

})