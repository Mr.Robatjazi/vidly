const  request = require('supertest');
const {Rental} = require('../../models/rental');
const mongoose = require('mongoose');

let server;
let rental;
let custimerId ;
let movieId;



describe('api/returns' , () => {

    beforeEach(async ()=> { 

        server=require('../../index');
        customerId =  new mongoose.Types.ObjectId();
        movieId = new mongoose.Types.ObjectId();

        rental = new Rental({
            customer: {
                _id: customerId,
                name: "12345",
                phone: "12345"
            },
            movie: {
                _id: movieId,
                title: "1234567",
                dailyRentalRate: 10
    
            }
    
        })
        await rental.save();
    });
    afterEach(async ()=> { 
        await server.close();
        await Rental.remove({});
    });

    it('should work', async () => {
        const result =await  Rental.findById(rental._id);
        expect(result).not.toBeNull();
    })
    


})