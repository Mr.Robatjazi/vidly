
const config = require('config');
const Joi = require('joi');

module.exports = function() {
    if (!config.get('jwtPrivateKey')) {
        throw new Error('FATAL! private key is not defined! ');
    }
    Joi.objectId = require('joi-objectid')(Joi);      
      
}