
// const moment = require('moment');
const {Rental} = require('../models/rental');
const {Movie} = require('../models/movie');
const {validate} =  require('../models/returns');
const auth = require('../middleware/authentication');
const validateMiddleware = require('../middleware/validate');
const express = require('express');
const router = express.Router();



router.post('/', [auth, validateMiddleware(validate)], async (req, res) => {
    // if (!req.body.movieId ){
    //     return res.status(400).send('movieId not provided');   
    // }
    
    // if (!req.body.customerId ){
    //     return res.status(400).send('customerId not provided');   
    // }
    // const { error } = validate(req.body); 
    // if (error) return res.status(400).send(error.details[0].message);
  

    // const rental = await Rental.findOne({
    //     'customer._id': req.body.customerId,
    //     'movie._id' : req.body.movieId
    // })
    const rental = await Rental.lookup(req.body.customerId, req.body.movieId);

    if (!rental) return res.status(404).send('The rental movie for this customet not found!');

    if (rental.dateReturned) return res.status(400).send('The rental already processed!');

    //if (!req.header('x-auth-token'))  return res.status(401).send('Unauthroized');            
    
    // rental.dateReturned= new Date();

    // var daysOut = moment().diff(rental.dateOut, 'days');
    // rental.rentalFee =  daysOut * rental.movie.dailyRentalRate ;
    rental.return();
    await rental.save();

    await Movie.update({ _id: req.body.movieId}, {
        $inc: {numberInStock: 1}       
    });

    return res.send(rental);


});

module.exports = router; 