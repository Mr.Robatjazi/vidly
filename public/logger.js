const moment = require('moment');
const logger = require('logger');
const config = require('config');

logStart= (logLevel) => {
    var log = logger.createLogger(`${config.get('logPath')}${moment().format('YYYY-MM-DD')}#${logLevel}.log`);
    log.format= function(level, date, message) {
        //return moment().format('YYYY/MM/DD HH:mm:ss:SSSS') + "; " + level + ' ; ' + message + '\r\n';
        return `${level};${ moment().format('YYYY/MM/DD HH:mm:ss:SSSS') }; ${message} \r\n `
    };
    return log;
} 



module.exports = {
    debug:  (source, msg)=> {
        //console.log(`[${moment().format('YYYY/MM/DD HH:mm:ss:SSSS')}][${source}]:[${msg}] `);
        log = logStart('debug');
        log.setLevel('debug');
        log.debug(source, msg);
    },
    info:  (source, msg)=> {
        //console.log(`[${moment().format('YYYY/MM/DD HH:mm:ss:SSSS')}][${source}]:[${msg}] `);
        log = logStart('info');
        log.setLevel('info');
        log.info(source, msg);
    },
    error: (source, msg)=> {
        log = logStart('error');
        log.setLevel('error');
        log.error(source, msg);
    },
    fatal: (source, msg)=> {
        log = logStart('fatal');
        log.setLevel('fatal');
        log.fatal(source, msg);
    }
    
}
